import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '../../globals';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersProvider {

  API = API;

  constructor(public http: HttpClient) {
    console.log('Hello UsersProvider Provider');
  }

  get(email=null){
    let endpoint
    if(email == null){
       endpoint = this.API + "User";
    }else{
      endpoint = this.API + `User?email=${email}`;
    }
    
    return this.http.get(endpoint);
  }

  create(body: any) {
    let endpoint = this.API + "User";
    let pbody = new FormData();

    pbody.append('username', body.username);
    pbody.append('password', body.password);
    pbody.append('email', body.email);
    pbody.append('age', body.age);
    pbody.append('sex', body.sex);
    pbody.append('phone', body.phone);

    //let headers = new Headers({});

    return this.http.post(endpoint, pbody);
  }

}

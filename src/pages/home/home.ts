import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReportDetailPage } from '../report-detail/report-detail';
import { ReportsProvider } from '../../providers/reports/reports';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { LandscapePage } from '../landscape/landscape';

import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  title: string = "Home";
  reportDetail: any = ReportDetailPage;
  
  items: any = [];
  constructor(public navCtrl: NavController, private reportsService: ReportsProvider, private storage: Storage, public platform: Platform, private screenOrientation: ScreenOrientation) {
    var last = (this.items.length < 1) ? 0 : this.items.length;
    this.getNewReports(last,2);
  }

  ionViewWillEnter(){
    //detectar si estoy en celular y si lo estoy forzar la orientación
    if (this.platform.is('cordova')) {
      try {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      } catch (error) {
        console.log(error);
      }
    }
  }

  getNewReports(last,q){
    last = (last < 0) ? 0 : last;
    this.reportsService.getNewOnes(last,q).subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) {
        this.items.push(data[i]);
      }
    });
  }

  doInfinite(infiniteScroll) {
    var last = (last < 0) ? 0 : last;
    this.reportsService.getNewOnes(last,2).subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) {
        this.items.push(data[i]);
      }
      infiniteScroll.complete();
    });
  }

  closeSession(){
    this.storage.set("user",null);
    this.navCtrl.setRoot(LoginPage);
  }

  landscapePage(){
    this.navCtrl.push(LandscapePage);
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandscapePage } from './landscape';

@NgModule({
  declarations: [
    LandscapePage,
  ],
  imports: [
    IonicPageModule.forChild(LandscapePage),
  ],
})
export class LandscapePageModule {}

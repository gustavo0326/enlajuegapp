import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the LandscapePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landscape',
  templateUrl: 'landscape.html',
})
export class LandscapePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, private screenOrientation: ScreenOrientation) {
  }

  ionViewWillEnter(){
    //detectar si estoy en celular y si lo estoy forzar la orientación
    if (this.platform.is('cordova')) {
      try {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      } catch (error) {
        console.log(error);
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandscapePage');
  }

}
